$(document).ready(function(){

    let showContributorsBtn = $('#show-contributors');
    let contribContainer = $('#angu-contrib');
  
    //logo move
    let logo  = $('.logo');

    function logoTransform(){
        logo.css({
            'width': '15vw',
            'height': '7vh',
            'top' : '2%',
            'left' : '1.7%',
            'transform': 'translate(0,0)',
            'background-color':'rgba(255, 255, 255, 1)',
            'box-shadow':'inset 0px 0px 60px 15px rgba(48,48,48,1)'
            
        });
    }

    setTimeout(function(){
        logoTransform();
    },7800);

    logo.click(function(){
        logoTransform();
    });

    
    //clear contribututors from HTML structure
    function clearContributors(){
        contribContainer.empty();
    }

    showContributorsBtn.click(function(){

        let $this = $(this);

        $this.toggleClass('show-contrib');

        if($this.hasClass('show-contrib')){
            $this.text('Show Angular Contributors Profiles');
            clearContributors();

        } else {
            $this.text('Hide Profiles');
            let repoContributors = [];
            //ajax calls needs to rebuild
            var getReposPage1 = $.ajax({
                url: `https://api.github.com/users/angular/repos?page=1`,
                data: {
                    client_id: '6114e1cf7c684aad9a33',
                    client_secret: 'cf28e715c7cca8d532f45872f0485708ffa3c556',
                    per_page: 3//change to 100 to retrieve all contributing users
                }
            });
            var getReposPage2 = $.ajax({
                url: `https://api.github.com/users/angular/repos?page=2`,
                data: {
                    client_id: '6114e1cf7c684aad9a33',
                    client_secret: 'cf28e715c7cca8d532f45872f0485708ffa3c556',
                    per_page: 3//change to 100 to retrieve all contributing users
                }
            });
            $.when(getReposPage1, getReposPage2).done(function(repos1, repos2){

                const allRepos = getReposPage1.responseJSON.concat(getReposPage2.responseJSON);
                // let informationArr = [];
                // let everyContribArr = [];
                //console.log(informationArr);
                //console.log(everyContribArr);
                //console.log(allRepos);

                $.each(allRepos, function(index, repo){
                    
                    const numberOfPages = 2 ;//change to 5 to retrieve all contributing users
                    //console.log(repo);
                    
                    for(let i = 1; i <= numberOfPages; i++){
                        //console.log(repo);
                        let url= repo.contributors_url + '?page='+ i;
                        $.ajax({
                            type: 'GET', 
                            url: url,
                            dataType: 'json',
                            data: {
                                client_id: '6114e1cf7c684aad9a33',
                                client_secret: 'cf28e715c7cca8d532f45872f0485708ffa3c556',
                                per_page: 5 //change to 100
                            }
                        }).done(function(contrib){
                            if(contrib == null){
                                console.log('pusta');
                            } else if (contrib != 0){
                                $.each(contrib, function(index, user){
                                    //console.log(contrib);
                                    //console.log(user);
                                    var followers = [];
                                    var repos = [];
                                    $.when(
                                        getFollowers(1),
                                        getRepos(1)
                                    );
                                    //everyContribArr.push(user);
                                    repoContributors.push({
                                        repoContributor: user.login,
                                        repoContributorAvatar: user.avatar_url,
                                        repoName: repo.name,
                                        contributions: user.contributions,
                                        followers: followers,
                                        repos: repos
                                    });
                                    
                                    function getRepos(page){
                                        $.ajax({
                                            type: 'GET',
                                            url: user.repos_url + '?page=' + page,
                                            dataType: 'json',
                                            data: {
                                                client_id: '6114e1cf7c684aad9a33',
                                                client_secret: 'cf28e715c7cca8d532f45872f0485708ffa3c556',
                                                per_page: 100 //change to 100
                                            },
                                            success: function(userRepos){
                                                //console.log(userRepos);
                                                repos.push(userRepos.length);
                                                if(userRepos.length < 100){
                                                    for(var r = 0; r < userRepos.length; r++){

                                                        repos.push(userRepos[r].name);
                                                    }
                                                } else if(userRepos.length === 100){
                                                    let nextPage = page + 1;
                                                    for(var rp = 0; rp < userRepos.length; rp++){
                                                        repos.push(userRepos[rp].name);
                                                    }
                                                    getRepos(nextPage);
                                                }                                                                    
                                            }
                                        });
                                    }
                                    function getFollowers(page){
                                        //console.log(page + 'main');
                                        //let perPage = 100;
                                        //let nextPage = page + 1;
                                        var request = $.ajax({
                                            type: 'GET',
                                            url: user.followers_url + '?page=' + page,
                                            dataType: 'json',
                                            data: {
                                                client_id: '6114e1cf7c684aad9a33',
                                                client_secret: 'cf28e715c7cca8d532f45872f0485708ffa3c556',
                                                per_page: 100
                                            },
                                            success: function(userFollowers){
                                                if(userFollowers.length < 100){
                                                    for(var f = 0; f < userFollowers.length; f++){
                                                        followers.push(userFollowers[f].login); 
                                                    }
                                                    //console.log(page  + '< 100');
                                                }
                                                //followers.push(userFollowers.length);
                                                
                                                else if(userFollowers.length === 100){
                                                    let nextPage = page + 1;
                                                    //console.log(nextPage + "next page");
                                                    for(var fp = 0; fp < userFollowers.length; fp++){
                                                        followers.push(userFollowers[fp].login); 
                                                    }
                                                    //console.log(page  + '=== 100');
                                                    getFollowers(nextPage);
                                                    
                                                }
                                            }
                                        });
                                    }                                                    
                                });                                                
                            }
                        });
                    }
                });

                $(document).ajaxStop(function () {      
                    function sort(a, b){
                        if(a.repoContributor < b.repoContributor){
                            return -1;
                        }  
                        if(a.repoContributor > b.repoContributor){
                            return 1;
                        }
                        return 0;
                    }
                    let sortedRepoContributors = repoContributors.sort(sort);
                    console.log(sortedRepoContributors);

                    //add contributors to HTML structure
                    function appendContributors(){
                        contribContainer.append(`
                            <div class="well bg-light">
                                <div class="row">
                                    <div class="col-3 col col-md-4">
                                        <div class="avatar">
                                            <img src=${sortedRepoContributors[i].repoContributorAvatar} alt="${sortedRepoContributors[i].repoConributor}"\>
                                        </div>
                                        <span class="label label-default">Login:</span>
                                        <a href="https://github.com/${sortedRepoContributors[i].repoContributor}" class="label label-primary" target="_blank">${sortedRepoContributors[i].repoContributor}</a>
                                    </div>
                                    <div class="col-5 col-md-4 ${sortedRepoContributors[i].repoContributor}">
                                            <div class="${sortedRepoContributors[i].repoContributor}-contribs contribs">                                             
                                                <span class="label btn-success user-contribs">${sortedRepoContributors[i].contributions} contributions in ${sortedRepoContributors[i].repoName}</span>
                                            </div>
                                    </div>
                                    <div class="col-4 col-md-4 ${sortedRepoContributors[i].repoContributor}">
                                            <div class="followers">
                                                <span class="label btn-info">Followers:</span>
                                                <span class="label btn-light">${sortedRepoContributors[i].followers.length}</span>
                                            </div>
                                            <div class="p-repos">
                                                <span class="label btn-info">Public repos:</span>
                                                <span class="label btn-light">${sortedRepoContributors[i].repos.length}</span>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        `);
                    }

                    function appendContributorsHalf(){
                        let divContrib = $(`.${sortedRepoContributors[i].repoContributor}-contribs`);
                        divContrib.append(`
                            <span class="label btn-success user-contribs">${sortedRepoContributors[i].contributions} contributions in ${sortedRepoContributors[i].repoName}</span>
                        `);
                    }
                    
                    //add contributor to HTML structure
                    let current = null;
                    
                    for(var i = 0; i < sortedRepoContributors.length; i++){                                        
                        if(sortedRepoContributors[i].repoContributor != current){
                            current = sortedRepoContributors[i].repoContributor;           
                            appendContributors();                                                      
                        }
                        else if(sortedRepoContributors[i].repoContributor === current){
                            appendContributorsHalf();                                     
                        }                                
                    }
                });
            });
            followers = [];
            repos = [];    
        }
    });
});

